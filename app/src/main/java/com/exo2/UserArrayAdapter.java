package com.exo2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserArrayAdapter extends  RecyclerView.Adapter<UserArrayAdapter.ViewHolder>{
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewLeft;
        TextView textViewRight;
        public ViewHolder( @NonNull View itemView){
            super(itemView);
            textViewLeft=itemView.findViewById(R.id.textViewLeft);
            textViewRight=itemView.findViewById(R.id.textViewRight);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context=parent.getContext();
        LayoutInflater inflater=LayoutInflater.from(context);
        View itemView=inflater.inflate(R.layout.users_user,parent,false);
        ViewHolder viewHolder=new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User users1=Singleton.getInstance().users.get(position);
        holder.textViewLeft.setText(users1.getUsername());
        holder.textViewRight.setText(""+users1.getCategory());
    }

    @Override
    public int getItemCount() {
        return Singleton.getInstance().users.size();
    }
}
