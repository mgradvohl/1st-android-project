package com.exo2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class ThirdActivity extends AppCompatActivity {

    EditText inscUserEditText;
    EditText inscPsswdEditText;
    EditText inscNameEditText;
    Spinner inscCategorySpinner;
    Button inscSaveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        setTitle("Add New User");

        inscUserEditText    = findViewById(R.id.inscUserEditText);
        inscPsswdEditText   = findViewById(R.id.inscPsswdEditText);
        inscNameEditText    = findViewById(R.id.inscNameEditText);
        inscCategorySpinner = findViewById(R.id.inscCategorySpinner);
        inscSaveButton      = findViewById(R.id.inscSaveButton);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.roles_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inscCategorySpinner.setAdapter(adapter);

        inscSaveButton.setOnClickListener(view -> {
            if(TextUtils.isEmpty(inscNameEditText.getText()) ||
              TextUtils.isEmpty(inscUserEditText.getText()) ||
              TextUtils.isEmpty(inscPsswdEditText.getText()) ) {
                Toast t3 = Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT);
                t3.show();

            } else {
                String username = inscNameEditText.getText().toString();
                String userid   = inscUserEditText.getText().toString();
                String password = inscPsswdEditText.getText().toString();
                String category = inscCategorySpinner.getSelectedItem().toString();

                addUser(userid, password, username, category);
                saveUsersToFile();
                finish();
            }
        });

    }

    private void addUser(String userid, String password, String username, String category) {
        User user = new User(userid, password, username, category);
        Singleton.getInstance().users.add(user);
    }
    private void saveUsersToFile(){
        try {
            OutputStream outputStream = openFileOutput("users.txt",
                    MODE_PRIVATE);
            OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream);
            for (User user:Singleton.getInstance().users) {
                streamWriter.write(user.getUserid()+"-"+
                                       user.getPassword()+"-"+
                                       user.getUsername()+"-"+
                                       user.getCategory()+"-"+
                                       user.getProfilepic());
                streamWriter.write("\n");
            }
            streamWriter.flush();
            streamWriter.close();
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}