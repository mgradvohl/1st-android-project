package com.exo2;

import java.util.ArrayList;
import java.util.Arrays;

public class Singleton {
    private static final Singleton instance = new Singleton();
    private Singleton() {

    }
    public static Singleton getInstance() {
        return instance;
    }
    public String userName = "";
    public String userCategory = "";
    public int profilePic = R.drawable.susuwatari;

    public ArrayList<User> users = new ArrayList<>();

}
