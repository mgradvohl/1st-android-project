package com.exo2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    EditText userEditText;
    EditText passwordEditText;
    Button loginButton;
    Button addUserButton;
    static boolean login = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recreateUsers();

        userEditText     = findViewById(R.id.userEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        loginButton      = findViewById(R.id.loginButton);
        addUserButton    = findViewById(R.id.addUserButton);

        loginButton.setOnClickListener(view -> {
            String user = userEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            verifyLogin(user, password);

        });

        addUserButton.setOnClickListener(view -> {
            createActivity(ThirdActivity.class);
            clearTexts();
        });
    }
    private void verifyLogin(String usr, String passwd) {
        for (User user:Singleton.getInstance().users) {
            if (usr.equals(user.getUserid())) {
                if (passwd.equals(user.getPassword())) {
                    login = true;
                    Singleton.getInstance().userName = user.getUsername();
                    Singleton.getInstance().userCategory = user.getCategory();
                    Singleton.getInstance().profilePic = user.getProfilepic();

                    if(user.getCategory().equals("Admin")){
                        createActivity(FourthActivity.class);
                    } else {
                        createActivity(SecondActivity.class);
                    }
                    clearTexts();
                    break;
                }
            }
        }
        if(!login) {
            Toast t2 = Toast.makeText(getApplicationContext(), "Wrong user or password", Toast.LENGTH_SHORT);
            t2.show();
        }
    }
    private void recreateUsers() {
        try{
            InputStream inputStream = openFileInput("users.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while((line = bufferedReader.readLine())!= null){
                String[] parts = line.split("-");
                String userid = parts[0];
                String password = parts[1];
                String username = parts[2];
                String category = parts[3];
                int profilepic = Integer.parseInt(parts[4]);

                addUser(userid, password, username, category, profilepic);
            }
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void addUser(String userid, String password, String username, String category, int profilepic) {
        User user = new User(userid, password, username, category, profilepic);
        Singleton.getInstance().users.add(user);
    }
    private void createActivity(Class Activity) {
        Intent intent = new Intent(MainActivity.this, Activity);
        startActivity(intent);
    }

    private void clearTexts() {
        userEditText.setText("");
        passwordEditText.setText("");
    }

    public static void setLogin(boolean info) {
        MainActivity.login = info;
    }
}