package com.exo2;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    ImageView userAvatarView;
    TextView userNameView;
    TextView userCategoryView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        setTitle("Welcome back, " + Singleton.getInstance().userName);
        userAvatarView = findViewById(R.id.userImageView);
        userNameView = findViewById(R.id.userNameView);
        userCategoryView = findViewById(R.id.userCategoryView);

        userAvatarView.setImageResource(Singleton.getInstance().profilePic);
        userNameView.setText(Singleton.getInstance().userName);
        userCategoryView.setText(Singleton.getInstance().userCategory);
    }

    @Override
    public void onBackPressed() {
        MainActivity.setLogin(false);
        finish();
    }
}