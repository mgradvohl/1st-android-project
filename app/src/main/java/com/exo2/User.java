package com.exo2;

import java.util.Random;

public class User {

    private String userid;
    private String password;
    private String username;
    private String category;
    private int profilepic;

    public User(String userid, String password, String username, String category, int profilepic)
    {
        this.userid = userid;
        this.password = password;
        this.username = username;
        this.category = category;
        this.profilepic = profilepic;

    }
    public User(String userid, String password, String username, String category)
    {
        this.userid = userid;
        this.password = password;
        this.username = username;
        this.category = category;
        this.profilepic = randomProfilePic();

    }
    public String getUserid() {
        return userid;
    }
    public String getPassword() {
        return password;
    }
    public String getUsername() {
        return username;
    }
    public String getCategory() {
        return category;
    }
    public int getProfilepic() {
        return profilepic;
    }

    private int randomProfilePic() {
        int profilePics[] = {R.drawable.chihiro, R.drawable.howl, R.drawable.mononoke,
                             R.drawable.ponyo, R.drawable.susuwatari, R.drawable.totoro};
        Random rand = new Random();

        return profilePics[rand.nextInt(6)];
    }
}
