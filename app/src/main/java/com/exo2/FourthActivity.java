package com.exo2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FourthActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    UserArrayAdapter arrayAdapter=new UserArrayAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setAdapter(arrayAdapter);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(com.exo2.FourthActivity.this)
        );

        setTitle("List of Users");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}